# Spherial Clock Arduino

This repository contains the source code and configuration files of the Arduino Core
for Atmel's SAMD21 processor (used on the SpherialClock board).

## Installation on Arduino IDE
1. Navigate to `File` -> `Preferences`

![Arduino IDE File -> Preferences](./doc/arduino_ide_file_pref.png)

2. Under `Aditional boards manager URLs` enter:
```
https://gitlab.ub.uni-bielefeld.de/api/v4/projects/7790/jobs/artifacts/main/raw/board.json?job=create-json
```
3. And confirm with `OK`

![Arduino IDE Additional boards](./doc/arduino_ide_additional_boards.png)

4. On the left, open the `Boards Manager`
5. Search for `spherical`
6. Install the `SphericalClock` Board

![Arduino IDE Board Manager](./doc/arduino_ide_board_manager.png)

## Prgramming the Board
1. 

## Board Documentation
![Kugeluhr PCB](./doc/kugeluhr_board_no_bg.png)
![Board Pinout](./doc/board_pinout.png)